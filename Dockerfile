FROM mongo:3.2
MAINTAINER Kevin Ger(bimfm.kvein@gmail)

# initilze
RUN apt-get -y update && \
    apt-get install -qqy pwgen netcat

RUN mkdir -p /usr/src/scripts
WORKDIR /usr/src/scripts

COPY scripts /usr/src/scripts
RUN chmod +x /usr/src/scripts/*.sh

ENTRYPOINT ["./setup.sh"]

CMD ["mongo"]

EXPOSE 27017 

VOLUME ["/data"]
