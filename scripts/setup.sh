#!/bin/sh

# Start MongoDB
echo "Starting MongoDB"
/usr/bin/mongod --dbpath /data --nojournal &
echo "Waitting MongoDB readyup"
while ! nc -vz localhost 27017; do 
	sleep 1; 
done

./create_user.sh

echo "Restart Mongo with authentiction"
/usr/bin/mongod --dbpath /data --shutdown

/usr/bin/mongod --dbpath /data --auth
	
